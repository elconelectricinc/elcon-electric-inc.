At Elcon Electric, we provide top quality electrical services to homeowners and businesses throughout Southeast Florida, as well as the Treasure Coast. Specializing in panel upgrades, parking lot lighting, and house rewires, all of our comes with a lifetime guarantee.

Address: 668 S. Military Trail, Deerfield Beach, FL 33442, USA

Phone: 800-446-8915